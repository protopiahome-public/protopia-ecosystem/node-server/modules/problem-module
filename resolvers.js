const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "solution";

module.exports = {

    Mutation:{
        changeProblem: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            if(args._id){
                return await query(collectionItemActor,  {"type": "problem", search:{_id: args._id}, input: args.input }, global.actor_timeout);
            }else {
                return await query(collectionItemActor, {"type": "problem", input: args.input}, global.actor_timeout);
            }

        },
    },

    Query: {

        getProblem: async (obj, args, ctx, info) => {


            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "problem", search: {_id: args.id}}, global.actor_timeout);


        },

        getProblems: async (obj, args, ctx, info) => {

            const collectionActor = ctx.children.get("collection")

            return await query(collectionActor, {"type": "problem"}, global.actor_timeout);

        },
    },
}